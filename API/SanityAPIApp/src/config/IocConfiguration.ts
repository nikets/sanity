﻿/// <reference path="../../typings/inversify/inversify.d.ts" />
/// <reference path="../app/repositories/email/EmailRepository.ts" />

var inversify = require("inversify");

import EmailService = require("../app/services/email/EmailService");
import EmailController = require("../app/controllers/email/EmailController");
import EmailRoute = require("../app/routes/EmailRoute");
import EmailRepository = require("../app/repositories/email/EmailRepository");
import HomeRoute = require("../app/routes/HomeRoute");
import HomeController = require("../app/controllers/HomeController");

class IocConfiguration {
    public static kernel: inversify.KernelInterface;

    static setup(): inversify.KernelInterface {
        var kernel = new inversify.Kernel();

        kernel.bind(new inversify.TypeBinding("IHomeRoute", HomeRoute, inversify.TypeBindingScopeEnum.Transient));
        kernel.bind(new inversify.TypeBinding("IHomeController", HomeController, inversify.TypeBindingScopeEnum.Transient));
        
        kernel.bind(new inversify.TypeBinding("IEmailRoute", EmailRoute, inversify.TypeBindingScopeEnum.Transient));
        kernel.bind(new inversify.TypeBinding("IEmailController", EmailController, inversify.TypeBindingScopeEnum.Transient));
        kernel.bind(new inversify.TypeBinding("IEmailService", EmailService, inversify.TypeBindingScopeEnum.Transient));
        kernel.bind(new inversify.TypeBinding("IEmailRepository", EmailRepository, inversify.TypeBindingScopeEnum.Transient));

        console.log("IOC register OK");
        return kernel;
    }
}

Object.seal(IocConfiguration);
export = IocConfiguration;