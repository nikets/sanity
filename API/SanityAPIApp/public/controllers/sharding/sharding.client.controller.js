﻿'use strict';

angular.module('emails').controller('ShardingController', ['$scope',
    '$routeParams', '$location', '$http',
    function ($scope, $routeParams, $location, $http){
        $scope.init = function() {
            // Get all documents in test collection
            $http.get('/api/shardings').success(function (response) {
                $scope.shardings = response.shardings;
            }).error(function (response) {
                $scope.error = response.message;
            });
        }
    }
]);