﻿/// <reference path="../../config/IocConfiguration.ts" />
import express = require("express");
import EmailController = require("../controllers/email/EmailController");
import IEmailController = require("../controllers/email/IEmailController");
import IocConfiguration = require("../../config/IocConfiguration");
import IEmailService = require("../services/email/IEmailService");
import EmailService = require("../services/email/EmailService");
import IEmailRoute = require("./IEmailRoute");
var inversify = require("inversify");
var router = express.Router();

class EmailRoute implements IEmailRoute {
    private _emailController: IEmailController;

    constructor(IEmailController: IEmailController) {
        //var emailService = new EmailService();
        //var emailService = IocConfiguration.resolve<IEmailService>("IEmailService");
        //this._emailController = new EmailController(emailService);
        this._emailController = IEmailController;
    }

    get routes() {
        var controller = this._emailController;
        router.get("/api/emails", controller.retrieve.bind(controller));
        router.post("/api/emails", controller.create.bind(controller));
        router.put("/api/emails/:_id", controller.update.bind(controller));
        router.get("/api/emails/:_id", controller.findById.bind(controller));
        router.delete("/api/emails/:_id", controller.delete.bind(controller));

        return router;
    }
}

Object.seal(EmailRoute);
export = EmailRoute;