var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var core_1 = require("angular2/core");
var loginActions_1 = require("../actions/loginActions");
var dispatcher_1 = require("../utils/dispatcher");
var LoginStore = (function (_super) {
    __extends(LoginStore, _super);
    function LoginStore() {
        var _this = this;
        _super.call(this);
        dispatcher_1.default.register(function (payload) {
            switch (payload.type) {
                case loginActions_1.LOGIN_SUCCESS:
                    console.log(payload.data);
                    break;
                default:
                    break;
            }
            _this.emit("changed");
        });
    }
    return LoginStore;
})(core_1.EventEmitter);
exports.LoginStore = LoginStore;
//# sourceMappingURL=loginStore.js.map