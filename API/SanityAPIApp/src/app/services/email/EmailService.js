var EmailService = (function () {
    function EmailService(IEmailRepository) {
        this._emailRepository = IEmailRepository;
    }
    EmailService.prototype.create = function (item, callback) {
        this._emailRepository.create(item, callback);
    };
    EmailService.prototype.retrieve = function (page, callback) {
        this._emailRepository.retrieve(page, callback);
    };
    EmailService.prototype.update = function (_id, item, callback) {
        var _this = this;
        this._emailRepository.findById(_id, function (err, res) {
            if (err)
                callback(err, res);
            else
                _this._emailRepository.update(res._id, item, callback);
        });
    };
    EmailService.prototype.delete = function (_id, callback) {
        this._emailRepository.delete(_id, callback);
    };
    EmailService.prototype.findById = function (_id, callback) {
        this._emailRepository.findById(_id, callback);
    };
    return EmailService;
})();
Object.seal(EmailService);
module.exports = EmailService;
//# sourceMappingURL=EmailService.js.map