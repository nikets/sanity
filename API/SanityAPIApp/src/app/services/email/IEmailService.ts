﻿import IBaseService = require("../IBaseService");
import EmailModel = require("../../models/email/EmailModel");

interface IEmailService extends IBaseService<EmailModel> {

}
export = IEmailService;