///<reference path="../../typings/body-parser/body-parser.d.ts"/>
///<reference path="../../typings/method-override/method-override.d.ts"/>
/// <reference path="../../typings/inversify/inversify.d.ts" />

import express = require('express');
//morgan = require('morgan'),
//compress = require('compression'),
import bodyParser = require('body-parser');
import methodOverride = require('method-override');
var inversify = require("inversify");
import IEmailRoute = require("../app/routes/IEmailRoute");
import IocConfiguration = require("./IocConfiguration");
import IHomeRoute = require("../app/routes/IHomeRoute");

class Configuration {
    static get setup() {
        var app = express();
        app.use(bodyParser.urlencoded({
            extended: true
        }));
        app.use(bodyParser.json());
        app.use(methodOverride());
    
        app.use(express.static('.'));
        app.set('views', './public/views');
        app.set('view engine', 'ejs');
        // register html extension with ejs view engine
        app.engine('html', require('ejs').renderFile);
        //app.set('view engine', 'html');

        var kernel: inversify.KernelInterface;
        kernel = IocConfiguration.setup();
        //TestConfig.setup();

        var homeRoute = kernel.resolve<IHomeRoute>("IHomeRoute");
        app.use("/", homeRoute.routes);

        var emailRoute = kernel.resolve<IEmailRoute>("IEmailRoute");
        app.use("/", emailRoute.routes);

        return app;
    }

}

Object.seal(Configuration);
export = Configuration;

//module.exports = function () {
//    var app = express();
//    //if (process.env.NODE_ENV === 'development') {
//    //    app.use(morgan('dev'));
//    //} else if (process.env.NODE_ENV === 'production') {
//    //    app.use(compress());
//    //}
//    app.use(bodyParser.urlencoded({
//        extended: true
//    }));
//    app.use(bodyParser.json());
//    app.use(methodOverride());
    
//    app.use(express.static('.'));
//    app.set('views', './public/views');
//    app.set('view engine', 'ejs');
//    // register html extension with ejs view engine
//    app.engine('html', require('ejs').renderFile);
//    //app.set('view engine', 'html');

//    require('../app/routes/index.server.routes.js')(app);
//    require('../app/routes/emails.server.routes.js')(app);
//    require('../app/routes/sharding.server.routes.js')(app);

//    return app;
//}