﻿   /// <reference path="../../../typings/tsd.d.ts" />

import mongoose = require("mongoose");

class RepositoryBase<T extends mongoose.Document> {

    private _model: mongoose.Model<mongoose.Document>;

    constructor(schemaModel: mongoose.Model<mongoose.Document>) {
        this._model = schemaModel;
    }

    create(item: T, callback: (error: any, result: any) => void) {
        this._model.create(item, callback);

    }

    retrieve(page: number, callback: (error: any, count: number, result: any) => void) {
        //this._model.find({}, callback);
        var pageSize = 5;
        this._model.find({}).skip((page - 1) * pageSize).limit(pageSize).exec(
            (err, result) => {
                this._model.count({}, (err, count) => callback(err, count, result));
            }
        );
    }

    update(_id: mongoose.Types.ObjectId, item: T, callback: (error: any, result: any) => void) {
        this._model.update({ _id: _id }, item, callback);

    }

    delete(_id: string, callback: (error: any, result: any) => void) {
        this._model.remove({ _id: this.toObjectId(_id) }, (err) => callback(err, null));

    }

    findById(_id: string, callback: (error: any, result: T) => void) {
        this._model.findById(_id, callback);
    }


    private toObjectId(_id: string): mongoose.Types.ObjectId {
        return mongoose.Types.ObjectId.createFromHexString(_id);
    }

}

export = RepositoryBase;